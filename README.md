My groups PiHat project is a instrument tuner. It consists of a range of push buttons, each corresponding to a specific tune (frequency). It operates using an onboard microphone. Essentially, the microphone will send an analog signal to the Pi, which will compare the mics signal to the specific tune (selected using push-buttons by the user), and indicate to the user whether the tune recorded is in or out of tune with the desired pitch.

Bill of materials: 
Pi microcontroller, 5x LEDs, buck regulator, op-amp, onboard microphone, 8x push buttons.

Cost: R100
